
            $(document).ready(function(){
                $("#covid19").modal('show');
                $("#covid19").on('shown.bs.modal', function(e){
                    console.log('El aviso de Covid se ha mostrado');
                });
                $("#covid19").on('hidden.bs.modal', function(e){
                    console.log('El aviso de Covid se ha aceptado');
                });
            });

            $(function(){                
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();
                $('.carousel').carousel({
                    interval: 3000
                });
                $('#contactar').on('show.bs.modal', function (e){
                    console.log('El modal se va a mostrar');
                    $('.btnContacto').removeClass('btn-outline-success');
                    $('.btnContacto').addClass('btn-primary');
                    $('.btnContacto').prop('disabled',true);


                });    
                $('#contactar').on('shown.bs.modal', function (e){
                    console.log('El modal se ha mostrado');
                });      
                $('#contactar').on('hide.bs.modal', function (e){
                    console.log('El modal se va a ocultar');
                }); 

                $('#contactar').on('hidden.bs.modal', function (e){
                    console.log('El modal se ha ocultado');
                    $('.btnContacto').removeClass('btn-primary');
                    $('.btnContacto').addClass('btn-outline-success');
                    $('.btnContacto').prop('disabled',false);
                });

            });
